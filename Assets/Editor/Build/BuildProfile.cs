﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
[CreateAssetMenu(fileName = "New build profile", menuName = "Build/New profile")]
public class BuildProfile : ScriptableObject {

    public int majorVersion;
    public int minorVersion;
    public int buildVersion;
    public string keystorePass;

    public BuildProfile(BuildProfile buildProfile)
    {
        majorVersion = buildProfile.majorVersion;
        minorVersion = buildProfile.minorVersion;
        buildVersion = buildProfile.buildVersion;
        keystorePass = buildProfile.keystorePass;
    }
    
}
