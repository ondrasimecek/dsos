﻿using System;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class BuildPlayer : MonoBehaviour {

    public static string[] scenes = new[] {
        "Assets/Scenes/Levels/VolcanicCave.unity"
    };
    private static string defaultProfilePath = "Assets/Editor/Build/Default.asset";

    #region ANDROID

    [MenuItem("Build/Android/Build Debug and Run")]
    public static void BuildAndroidDebugAutoRun()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        string buildPath = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        if (string.IsNullOrEmpty(buildPath))
            return;

        EditorUserBuildSettings.buildAppBundle = true;
        EditorUserBuildSettings.androidCreateSymbolsZip = false;

        // debug build with mono
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.Mono2x);
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7;

        buildPlayerOptions.scenes = GetScenesToBuild();
        buildPlayerOptions.locationPathName = buildPath + "/" + DateTime.Now.ToString("yyyyMMdd") + ".aab";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.AutoRunPlayer | BuildOptions.Development;

        DoBuild(buildPlayerOptions);
    }

    [MenuItem("Build/Android/Build And Run")]
    public static void BuildAndroidAutoRun()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        string buildPath = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        if (string.IsNullOrEmpty(buildPath))
            return;

        EditorUserBuildSettings.buildAppBundle = true;
        EditorUserBuildSettings.androidCreateSymbolsZip = false;

        // build with il2cpp for all targets
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

        buildPlayerOptions.scenes = GetScenesToBuild();
        buildPlayerOptions.locationPathName = buildPath + "/" + DateTime.Now.ToString("yyyyMMdd") + ".aab";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.AutoRunPlayer;

        DoBuild(buildPlayerOptions);
    }

    [MenuItem("Build/Android/Build Only/Google Play")]
    public static void BuildAndroidOnly()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        string buildPath = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        if (string.IsNullOrEmpty(buildPath))
            return;

        EditorUserBuildSettings.buildAppBundle = true;
        EditorUserBuildSettings.androidCreateSymbolsZip = false;

        // build with il2cpp for all targets
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

        buildPlayerOptions.scenes = GetScenesToBuild();
        buildPlayerOptions.locationPathName = buildPath + "/" + DateTime.Now.ToString("yyyyMMdd") + ".aab";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        DoBuild(buildPlayerOptions);
    }

    [MenuItem("Build/Android/Build Only/UDP")]
    public static void BuildAndroidOnlyUDP()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();

        string buildPath = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        if (string.IsNullOrEmpty(buildPath))
            return;

        EditorUserBuildSettings.buildAppBundle = false;
        EditorUserBuildSettings.androidCreateSymbolsZip = false;

        // build with il2cpp for all targets
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

        buildPlayerOptions.scenes = GetScenesToBuild();
        buildPlayerOptions.locationPathName = buildPath + "/" + DateTime.Now.ToString("yyyyMMdd") + ".apk";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        DoBuild(buildPlayerOptions);
    }

    private static void SetAndroidAppVersion()
    {
        PlayerSettings.Android.bundleVersionCode = GetBuildVersionAsInt();
        PlayerSettings.bundleVersion = GetBuildVersionAsString();

        Debug.Log("Build version int: " + PlayerSettings.Android.bundleVersionCode + " | bundle version string: " + PlayerSettings.bundleVersion);
    }

    #endregion

    private static void DoBuild(BuildPlayerOptions buildPlayerOptions)
    {
        DateTime now = DateTime.Now;

        if (!IncreaseBuildVersion())
            return;

        if (buildPlayerOptions.target == BuildTarget.Android)
            SetAndroidAppVersion();

        string keystorePass = GetKeyStorePass();
        PlayerSettings.keystorePass = keystorePass;
        PlayerSettings.keyaliasPass = keystorePass;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
            TimeSpan timePassed = DateTime.Now - now;
            Debug.Log("Build took " + timePassed.Minutes.ToString("00") + ":" + timePassed.Seconds.ToString("00") + "." + timePassed.Milliseconds.ToString("000") + " to finish");
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
        }
    }

    #region BUILD VERSION

    private static string GetBuildVersionAsString()
    {
        BuildProfile buildProfile = AssetDatabase.LoadAssetAtPath<BuildProfile>(defaultProfilePath);

        if(buildProfile == null)
        {
            Debug.LogError("Build profile not found");
            return null;
        }

        return buildProfile.majorVersion + "." + buildProfile.minorVersion + "." + buildProfile.buildVersion;
    }

    private static int GetBuildVersionAsInt()
    {
        BuildProfile buildProfile = AssetDatabase.LoadAssetAtPath<BuildProfile>(defaultProfilePath);

        if(buildProfile == null)
        {
            Debug.LogError("Build profile not found");
            return 0;
        }

        return (buildProfile.majorVersion * 1000000) + (buildProfile.minorVersion * 10000) + buildProfile.buildVersion;
    }

    private static string GetKeyStorePass()
    {
        BuildProfile buildProfile = AssetDatabase.LoadAssetAtPath<BuildProfile>(defaultProfilePath);

        if (buildProfile == null)
        {
            Debug.LogError("Build profile not found");
            return null;
        }

        return buildProfile.keystorePass;
    }

    private static string[] GetScenesToBuild()
    {
        string[] scenesToBuild = new string[EditorBuildSettings.scenes.Length];
        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            scenesToBuild[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenesToBuild;
    }

    private static bool IncreaseBuildVersion()
    {
        BuildProfile buildProfile = AssetDatabase.LoadAssetAtPath<BuildProfile>(defaultProfilePath);

        if(buildProfile == null)
        {
            Debug.LogError("Build profile not found");
            return false;
        }

        buildProfile.buildVersion++;

        BuildProfile newBuildProfile = new BuildProfile(buildProfile);
        
        // save profile changes
        AssetDatabase.DeleteAsset(defaultProfilePath);
        AssetDatabase.CreateAsset(newBuildProfile, defaultProfilePath);

        return true;
    }

    #endregion
}
