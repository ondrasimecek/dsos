﻿using UnityEngine;

[RequireComponent(typeof(Transform))]
public class AppleiPadTransformAdjust : MonoBehaviour
{
    public Vector3 transformPosition;

    private void Awake()
    {
        float screenRatio = (float)Screen.width / Screen.height;
        if (screenRatio < 1.4f)
        {
            Transform transform = GetComponent<Transform>();
            transform.localPosition = transformPosition;
        }
    }
}
