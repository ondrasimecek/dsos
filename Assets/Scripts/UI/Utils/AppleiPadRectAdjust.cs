﻿using UnityEngine;

public class AppleiPadRectAdjust : MonoBehaviour
{
    public RectTransform rectTransform;

    [Header("Anchors")]
    public Vector2 anchorMin;
    public Vector2 anchorMax;

    public bool overrideAnimation = false;

    [ContextMenu("Adjust")]
    void Start()
    {
        float screenRatio = (float)Screen.width / Screen.height;
        if (screenRatio < 1.4f)
        {
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;

            rectTransform.offsetMin = new Vector2();
            rectTransform.offsetMax = new Vector2();
        }
    }

    private void LateUpdate()
    {
        if (!overrideAnimation)
            return;

        float screenRatio = (float)Screen.width / Screen.height;
        if (screenRatio < 1.4f)
        {
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
        }
    }
}
