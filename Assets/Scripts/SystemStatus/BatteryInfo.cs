using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BatteryInfo : MonoBehaviour
{
    public TextMeshProUGUI batteryPercentage;

    [Header("Graphics")]
    [Tooltip("Icon on button or panel")]
    public Image icon;
    [Tooltip("Outline or background of panel or button")]
    public Image infoOutline;
    [Tooltip("Color when device is charging")]
    public Color charging;
    [Tooltip("Color when device battery is high")]
    public Color highColor;
    [Tooltip("Number in percent when medium")]
    public int mediumThreshold = 50;
    [Tooltip("Color when device battery is medium")]
    public Color mediumColor;
    [Tooltip("Number in percent when low")]
    public int lowThreshold = 20;
    [Tooltip("Color when device battery is low")]
    public Color lowColor;

    private IEnumerator Start()
    {
        // check and update every 30 seconds
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(30f);
        while (true)
        {
            // get system info - battery level
            int batteryStatusInPercent = (int)(SystemInfo.batteryLevel * 100);
            batteryPercentage.text = batteryStatusInPercent.ToString("0") + "%";

            // when charging
            if(SystemInfo.batteryStatus == BatteryStatus.Charging)
            {
                batteryPercentage.color = charging;
                icon.color = charging;
                infoOutline.color = charging;
            }
            else
            {
                // when status
                if(batteryStatusInPercent <= lowThreshold)
                {
                    // low
                    batteryPercentage.color = lowColor;
                    icon.color = lowColor;
                    infoOutline.color = lowColor;
                } 
                else if(batteryStatusInPercent <= mediumThreshold)
                {
                    // medium
                    batteryPercentage.color = mediumColor;
                    icon.color = mediumColor;
                    infoOutline.color = mediumColor;
                } 
                else
                {
                    // high
                    batteryPercentage.color = highColor;
                    icon.color = highColor;
                    infoOutline.color = highColor;
                }
            }

            yield return wait;
        }
    }
}
