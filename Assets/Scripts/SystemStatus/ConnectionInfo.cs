using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionInfo : MonoBehaviour
{
    public TextMeshProUGUI connection;

    [Header("Graphics")]
    [Tooltip("Icon on button or panel")]
    public Image icon;
    [Tooltip("Outline or background of panel or button")]
    public Image infoOutline;
    [Tooltip("Color when device is connected")]
    public Color connectedColor;
    [Tooltip("Color when device is disconnected")]
    public Color disconnectedColor;

    private IEnumerator Start()
    {
        // check and update every 5 seconds
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(5f);
        while (true)
        {
            // when disc
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                connection.text = "Nepřipojeno";
                connection.color = disconnectedColor;
                icon.color = disconnectedColor;
                infoOutline.color = disconnectedColor;
            }
            else
            {
                // when conn
                if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
                {
                    // wifi
                    connection.text = "WIFI";
                    connection.color = connectedColor;
                    icon.color = connectedColor;
                    infoOutline.color = connectedColor;
                }
                else
                {
                    // mobile
                    connection.text = "Mobilní data";
                    connection.color = connectedColor;
                    icon.color = connectedColor;
                    infoOutline.color = connectedColor;
                }
            }

            yield return wait;
        }
    }
}
