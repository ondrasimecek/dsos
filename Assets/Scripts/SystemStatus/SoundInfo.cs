using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SoundInfo : MonoBehaviour
{
    public TextMeshProUGUI volume;

    private IEnumerator Start()
    {
        // check and update every 5 seconds
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(5f);
        while (true)
        {
            volume.text = ((GetDeviceVolume() / (float)GetDeviceMaxVolume()) * 100).ToString("0") + "%";

            yield return wait;
        }
    }

    private static int GetDeviceVolume()
    {
        try
        {
            int streammusic = 3;
            return deviceAudio.Call<int>("getStreamVolume", streammusic);
        }
        catch (System.Exception)
        {
            return -1;
        }
    }

    private static int GetDeviceMaxVolume()
    {
        try
        {
            int streammusic = 3;
            return deviceAudio.Call<int>("getStreamMaxVolume", streammusic);
        }
        catch (System.Exception)
        {
            return -1;
        }
    }

    /*private static void SetDeviceVolume(int value)
    {
        int streammusic = 3;
        int flagshowui = 0;

        deviceAudio.Call("setStreamVolume", streammusic, value, flagshowui);
    }*/

    private static AndroidJavaObject audioManager;

    private static AndroidJavaObject deviceAudio
    {
        get
        {
            if (audioManager == null)
            {
                AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject context = up.GetStatic<AndroidJavaObject>("currentActivity");

                string audioName = context.GetStatic<string>("AUDIO_SERVICE");

                audioManager = context.Call<AndroidJavaObject>("getSystemService", audioName);
            }
            return audioManager;
        }
    }
}
