using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeDateInfo : MonoBehaviour
{
    public TextMeshProUGUI time;
    public TextMeshProUGUI date;

    private IEnumerator Start()
    {
        // check and update every 30 seconds
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(30f);
        while (true)
        {
            // use system datetime
            DateTime now = DateTime.Now;
            time.text = now.ToString("HH:mm");

            date.text = now.ToString("ddd d. M. yyyy");

            yield return wait;
        }
    }
}
